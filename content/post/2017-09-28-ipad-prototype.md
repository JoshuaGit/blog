---
title: Paper prototype iteratie 2
subtitle: Een kartonnen iPad!
date: 2017-09-28
---

De ochtend begon voor mij weer met Tools for Design (Sketch). Elke week begrijp ik het programma steeds meer en ik vind het een tot nu toe nog steeds een goede toevoeging. Na de les hebben we weer afgesproken met het team, waarbij we het paper prototype zouden maken. In deze iteratie hebben we gekozen om in plaats van een iphone een ipad te gebruiken in ons spel. Met 1 ipad mini in het team vonden wij het ideaal voor de interactie van ons spel door het formaat, een duidelijk groot scherm maar niet te groot waardoor het toch nog handig is om mee te lopen in de stad. Het gebruik van karton was wederom een succes. Iedereen had een goede bijdrage in de vormgeving van de schermen.  