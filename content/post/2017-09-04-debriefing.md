---
title: Debriefing
subtitle: En de doelgroepkeuze en onderzoekvragen!
date: 2017-09-04
---

De ochtend begon weer met een korte introductie, hierna hebben we elkaar onderzoeksvragen met elkaar gedeeld. Om verder te kunnen met de onderzoeksvragen hebben we een doelgroep gekozen die wij passend vonden en waar wij niet veel van wisten zodat we met een frisse blik onderzoek konden doen. (Doelgroepkeuze: Fysiotherapie) Hierna hebben we de onderzoeksvragen verder uitgewerkt en verdeeld, wie wat zou oppakken. We hebben de briefing ook ge de-brieft en op basis van de deliverbales een planning gemaakt met een scrumboard. We hebben ook een dropbox aangemaakt met de groep zodat we alle bestanden samen konden verzamelen en opslaan. We zijn allemaal begonnen met de onderzoeksvragen en zouden hier woensdag mee verder gaan. 
