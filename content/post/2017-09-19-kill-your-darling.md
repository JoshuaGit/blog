---
title: Kill your darling!
subtitle: Start iteratie 2
date: 2017-09-19
---

Vandaag kregen we de verwachte briefing voor iteratie 2. Dit was een erg bijzonder moment omdat veel mensen geschokt reageerden omdat we ons eerste concept eigenlijk een beetje moesten vergeten en een nieuw concept moesten bendenken. Ik vond dit niet erg en vond het eigenlijk wel leuk om opnieuw te beginnen. Ik merkte ook gelijk dat we het ontwerp proces hierdoor beter aanpakte omdat we al van de eerste iteratie geleerd hadden. We hebben vandaag onze moodboards verbeterd en meer informatie verzameld over onze doelgroep. 