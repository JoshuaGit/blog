---
title: First day of school, info + creatieve opdracht
subtitle: Motivated!
date: 2017-08-30
---

Vandaag kregen we eerst een presentatie over wat we kunnen verwachten van dit jaar, daarna zochten we de studio's op. In de studio's kregen we uitleg over wat de afspraken waren binnen de studio. Vervolgens kregen we uitleg over een creatieve opdracht die je met je gekozen groepje van kamp moest doen: een 3D logo maken. Dit logo moest je team uitbeelden, met verschillende materialen hebben we dit logo gemaakt en gepresenteerd aan de andere groepen. 
