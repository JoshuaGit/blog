---
title: Eerste hoorcollege
subtitle: Design Theory
date: 2017-09-05
---

Vandaag hadden we ons eerste hoorcollege Design Theory gehad, daarbij werd het design proces uitgelegd aan de hand van een driehoek die elkaar verbind met onderzoek, prototype en concept. Met deze methode kun je antwoorden krijgen op je vraagstellingen en creëer je van een huidige situatie een toekomstige gewenste situatie. 
