---
title: Werkcollege
subtitle: Het ontwerpproces in een dagelijkse situatie
date: 2016-03-08
---

Donderdag hebben we alleen een werkcollege die aansluitend is op het hoorcollege van dinsdag. We moesten ons met ons groepje afvragen of we in het dagelijks leven ook een keer het ontwerp proces hebben beleefd “van huidige situatie naar gewenste situatie’’. En dat heeft iedereeen bewust of onbewust een keer meegemaakt. Ik had het met het geven van een feestje van 3 vrienden en ik voor 120 man meegemaakt. We kozen uiteindelijk ook voor mijn situatie en hebben deze aan de hand van schetsen het proces in beeld gebracht op een A3 en deze gepresenteerd aan de andere groepjes.
