---
title: Tools for Design
subtitle: Sketch
date: 2017-09-14
---

Vandaag was de eerste 'Tools for Design' les. Hierna hadden we weer met het team afgesproken om onze spelanlyse af te ronden. Ik had voor de 'Tools for Design' gekozen voor Sketch omdat ik nog nooit eerder had gewerkt met dit programma (Photoshop en Illustrator wel), en omdat het me leuk lijkt om in Sketch te gaan ontwerpen omdat veel web/app designers dit doen. In de middag hadden we alle deliverables voor itteratie 1 af en hebben we deze in 1 document ingeleverd op N@tschool.