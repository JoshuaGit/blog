---
title: Het concept liep even vast..
subtitle: Maar door een leerzaam proces konden we weer verder!
date: 2017-09-11
---

De dag begonnen we met een stand-up om te bespreken hoe ver iedereen was en of iemand nog tegen een probleem aanliep. Op het scrumbord keken we wat er vandaag belangerijk was, we zagen dat het concept vandaag eigelijk afgerond moest worden. Als het concept af was konden we gaan beginnen aan de paper prototype. We deden erg ons best maar we liepen vast in onze inspiratie. We besloten om hulp te vragen aan de project docent Bob. Hij ons hier erg geholpen door op een andere manier te kijken naar ons concept. Hij liet ons ook kijken naar een object om daar samen over te brainstormen en dan merk je dat je in een team echt veel kan bedenken samen. Dit was een erg leerzaam proces. 